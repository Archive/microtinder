#!/bin/bash

# starts a jhbuild tinderbox, and keeps doing it, cleaning things out
# between runs.

# use 'tinder.sh test' to not start from scratch every time- useful
# when you're debugging build failures the first time through.

# this should end up in your Atom feed
export MY_TINDER_URL=http://music.local/tinderbox/

while
    true
do

echo 'Starting a new build.'

#=====================================================================
#
# 0. make sure we are starting fresh, if this is not a test
#
#=====================================================================

if [ "$1" != "test" ]; then
  echo 'Removing the previous checkout and build.'
  rm -rf cvs build-output
  mkdir cvs
fi

#=====================================================================
#
# 1. copy in a new bootstrapped root
#
#=====================================================================

  #FIXME: would be good to check and see if the bootstrap needs updating
  #TODO: copy in cvs/ pre-seeded with mozilla?

if [ "$1" != "test" ]; then
  echo 'Copying in bootstrap output.'
  cp -r build-output-bootstrap-backup build-output
fi

#=====================================================================
#
# 2. update the moduleset
#
#=====================================================================

  echo 'Updating jhbuild.'
  cd jhbuild/modulesets/
  cvs up -A 
  cd ../../

#=====================================================================
#
# 3. create the logging directory and set up appropriate symlinks
#
#=====================================================================

  echo 'Setting up the logging output.'
  cd web-output/
  export START_TIME=`date +%Y%m%d%H%M`
  mkdir $START_TIME
  rm ONGOING
  ln -s $START_TIME ONGOING
  cp LATEST/atom.xml ONGOING
  cd ..

#=====================================================================
#
# 4. do the build
#
#=====================================================================

  echo 'Here we go.'
  jhbuild -f jhbuildrc tinderbox -o web-output/$START_TIME -u $MY_TINDER_URL$START_TIME/

#=====================================================================
#
# 5. update symlinks
#
#=====================================================================

  cd web-output/
  rm LATEST
  ln -s $START_TIME LATEST
  cd ..

#=====================================================================
#
# 6. clean up after ourselves, if this is not a test
#
#=====================================================================

if [ "$1" != "test" ]; then
  echo 'Clean up after ourselves.'
  rm -rf cvs build-output
  mkdir cvs
fi

done
