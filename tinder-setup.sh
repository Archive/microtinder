#!/bin/bash

# script to make sure everything is ready to go for tinder.sh

#=====================================================================
#
# 1. make appropriate directories
#
#=====================================================================

mkdir cvs tarballs build-output web-output

#=====================================================================
#
# 2. update and install jhbuild
#
#=====================================================================

# apply all patches in jhbuild-patches. If it doesn't apply, shouldn't
# be there.

for i in `ls jhbuild-patches/*patch`;
do
  patch -p0 < $i
done

# FIXME: 'make install' should install to PWD/bin/, and tinder.sh should
#        add PWD/bin/ to PATH so this is more self-contained.

cd jhbuild
make install
cd ..

#=====================================================================
#
# 3. bootstrap
#
#=====================================================================

jhbuild -f jhbuildrc bootstrap
mv build-output build-output-bootstrap-backup

#=====================================================================
#
# 4. We gonna party like it's your birthday.
#
#=====================================================================

echo 'Assuming there are no errors above, you are bootstrapped.  
Now run "tinder.sh test" until your dep problems are straightened out.
Next, arrange to publicly export the "web-output" dir via a symlink or scp.
Finally, run "tinder.sh", and congrats, you are a tinderbox.'
